<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_coursesearch',
    'Coursesearch',
    [
        \DRK\DrkCoursesearch\Controller\CourseSearchController::class => 'form',
    ],
    // non-cacheable actions
    [
        \DRK\DrkCoursesearch\Controller\CourseSearchController::class => 'form',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_coursesearch',
    'CoursesearchResult',
    [
        \DRK\DrkCoursesearch\Controller\CourseSearchResultController::class => 'courseResult',
    ],
    // non-cacheable actions
    [
        \DRK\DrkCoursesearch\Controller\CourseSearchResultController::class => 'courseResult',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
