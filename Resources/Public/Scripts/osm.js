$.getScript( osmPath, function ($) {
  'use strict';


  OpenLayers.Lang.setCode('de');

  let map = new OpenLayers.Map('map', {units: 'meters'});

  let mapnik         = new OpenLayers.Layer.OSM(
    'OpenStreetMap',
    // Official OSM tileset as protocol-independent URLs
    [
                                              'https://a.tile.openstreetmap.org/${z}/${x}/${y}.png',
                                              'https://b.tile.openstreetmap.org/${z}/${x}/${y}.png',
                                              'https://c.tile.openstreetmap.org/${z}/${x}/${y}.png'
    ],
    null);
  let fromProjection = new OpenLayers.Projection('EPSG:4326');   // Transform from WGS 1984
  let toProjection   = new OpenLayers.Projection('EPSG:900913'); // to Spherical Mercator Projection

  let markers = new OpenLayers.Layer.Markers( 'Markers' );

  let bounds = new OpenLayers.Bounds();
  let i = 0;

  for (i = 0; i < locations.length; i++) {
    let iconSize = new OpenLayers.Size(32,32);
    let iconOffset = new OpenLayers.Pixel(-(iconSize.w/2), -iconSize.h);
    let icon = new OpenLayers.Icon(mapMarkerImg, iconSize, iconOffset);

    let marker = new OpenLayers.Marker(
      new OpenLayers.LonLat(locations[i][2],locations[i][1]).transform(fromProjection, toProjection),
      icon
    );

    let label = document.createElement('span');
    label.className = 'marker-label';
    label.innerHTML = locations[i][3];

    marker.icon.imageDiv.appendChild(label);

    markers.addMarker(marker);
    bounds.extend(marker.lonlat);
  }

  map.addLayers([mapnik, markers]);
  map.zoomToExtent(bounds);

});

