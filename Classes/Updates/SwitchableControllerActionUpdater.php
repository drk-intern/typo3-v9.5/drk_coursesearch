<?php
namespace DRK\DrkCoursesearch\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_cousesearch-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkcoursesearch_coursesearch',
            'switchableControllerActions' => 'Course->form;Course->courseResult',
            'targetListType' => '',
            'targetCtype' => 'drkcoursesearch_coursesearch'
        ], [
            'sourceListType' => 'drkcoursesearch_coursesearch',
            'switchableControllerActions' => 'Course->courseResult',
            'targetListType' => '',
            'targetCtype' => 'drkcoursesearch_coursesearchresult'
        ],
    ];
}
