<?php

namespace DRK\DrkCoursesearch\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;

class CourseRepository extends AbstractDrkRepository
{

    /**
     * @param bool $sortAlphabetically
     *
     * @return array
     */
    public function getCourseTypeList($sortAlphabetically = false)
    {
        $courseClient = $this->getCourseClient();

        if (empty($courseClient)) {
            return [];
        }

        $courses = Utility::convertObjectToArray(
            $courseClient->getCourseTypeList()->CourseTypes
        );

        if (empty($courses)) {
            return [];
        }

        if ($sortAlphabetically == true) {
            usort($courses, function ($a, $b) {
                return strcmp($a["CourseTypeDescription"], $b["CourseTypeDescription"]);
            });
        }

        return $courses;
    }

    /**
     * @param     $courseType
     * @param     $where
     * @param int $range
     *
     * @return array
     * @throws \Exception
     */
    public function getCourses($courseType, $where, $range = 5)
    {
        $cacheIdentifier = sha1(json_encode(['drk_coursesearch|getCourses',$courseType, $where, $range]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $courses = Utility::convertObjectToArray(
            $this->getCourseClient()->getCourses($where, $range, $courseType)->Courses
        );

        if (empty($courses)) {
            return [];
        }

        Utility::sortCourseArrayByDate($courses);

        $latLngArray = [];
        $latLngCounter = 0;

        foreach ($courses as &$course) {
            $curseGeoHash = $course['Latitude'] . ':' . $course['Longitude'];
            if (($latLngIndex = array_search($curseGeoHash, $latLngArray)) !== false) {
                $currentAsciiChar = $latLngIndex;
            } else {
                $latLngArray[] = $curseGeoHash;
                $currentAsciiChar = $latLngCounter++;
            }
            $course['markerLabel'] = chr($currentAsciiChar + 65);

            $course['Date'] = Utility::generateCourseDates($course, false);
        }

        $this->getCache()->set($cacheIdentifier, $courses);

        return $courses;
    }

    /**
     * @param        $where
     * @param string $sortingBy
     * @param string $orderBy
     *
     * returns only a organisation like Kreisverband
     *
     * @return array
     */
    public function getOrganisation($where, $sortingBy = '', $orderBy = '')
    {
        $aOrgResult = [];

        $cacheIdentifier = sha1(json_encode(['drk_coursesearch|getOrganisation', $where, $sortingBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $aOrganisations = Utility::convertObjectToArray(
            $this->getDldbClient()->getOrganisationbyZiporCity($where, $sortingBy, $orderBy)
        );

        if (!empty($aOrganisations)) {
            // first iteration to find a suitable city name
            foreach ($aOrganisations as $aOrganisation) {
                if ($this->__isZip($where) && $aOrganisation['orgType'] == 'K' &&
                    strstr($aOrganisation['orgZipcode'], $where))
                {
                    $aOrgResult = $aOrganisation;
                }
                elseif ($aOrganisation['orgType'] == 'K' &&
                    strstr($aOrganisation['orgCity'], $where))
                {
                    $aOrgResult = $aOrganisation;
                }
            }
            // if no result, second iteration to find a suitable organisation name
            if (is_array($aOrgResult) && empty($aOrgResult)) {
                foreach ($aOrganisations as $aOrganisation) {
                    if ($aOrganisation['orgType'] == 'K' && strstr($aOrganisation['orgSubName'], $where)) {
                        $aOrgResult = $aOrganisation;
                    }
                }
            }
            // if no result, third iteration to find a suitable organisation
            if (is_array($aOrgResult) && empty($aOrgResult)) {
                foreach ($aOrganisations as $aOrganisation) {
                    if ($aOrganisation['orgType'] == 'K') {
                        $aOrgResult = $aOrganisation;
                    }
                }
            }
            // if no result, return given organisation
            if (is_array($aOrgResult) && empty($aOrgResult)) {
                $aOrgResult = current($aOrganisations);
            }

        }

        $this->getCache()->set($cacheIdentifier, $aOrgResult, [], $this->long_cache);

        return $aOrgResult;
    }

    /**
     * ItemsProcFunc for Coursesearch->settings.flexforms.preselection
     *
     * @param array $parameters
     * @return array
     */
    public function addCourseListItems(array $parameters)
    {
        $this->initSettingsForListItems($parameters);
        foreach ($this->getCourseTypeList() as $course) {
            $parameters['items'][] = [
                $course['CourseTypeDescription'],
                $course['CourseType'],
                '',
            ];
        }

        return $parameters;
    }

    /**
     * returns true, if a numeric string is given and it's at most 5 chars or,
     * if $bStrict is true, exactly 5 chars long
     * @access private
     * @param string $sString
     * @param bool $bStrict
     * @return bool
     */
    protected function __isZip(string $sString, bool $bStrict = false): bool {
        return is_numeric($sString) && ($bStrict ? strlen($sString) === 5 : strlen($sString) <= 5);
    }

}
