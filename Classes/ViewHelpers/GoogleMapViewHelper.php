<?php

namespace DRK\DrkCoursesearch\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Page\AssetCollector;

//TODO: reconsider use of Google API key
class GoogleMapViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    protected array $settings;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @var string
     */
    protected string $googleApiKey;

    /**
     * @var string
     */
    protected string $googleMapsApiUrl;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Initialize arguments
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArguments([
            ['locations', 'string', 'The locations to show on the map', true, null],
            ['latitudeField', 'string', 'The identifier for latitude', false, 'Latitude'],
            ['longitudeField', 'string', 'The identifier for longitude', false, 'Longitude'],
            ['height', 'string', 'The height', false, 440],
            ['width', 'string', 'The width', false, null],
            ['class', 'string', 'The class', false, null],
            ['alt', 'string', 'The alt', false, null],
            ['title', 'string', 'The title', false, null]
        ]);
    }

    /**
     * @param array $arguments
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    protected function registerArguments(array $arguments): void
    {
        if (is_array($arguments)) {
            foreach ($arguments as $argument) {
                if (is_array($argument)) {
                    $this->registerArgument($argument[0] ?? '', $argument[1] ?? '', $argument[2] ?? '', $argument[3] ?? false, $argument[4] ?? null);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getGoogleApiKey(): string
    {
        return $this->googleApiKey;
    }

    /**
     * @param string $googleApiKey
     */
    public function setGoogleApiKey($googleApiKey): void
    {
        $this->googleApiKey = $googleApiKey;
    }

    /**
     * @return string
     */
    public function getGoogleMapsApiUrl(): string
    {
        return $this->googleMapsApiUrl;
    }

    /**
     * @param string $googleMapsApiUrl
     */
    public function setGoogleMapsApiUrl($googleMapsApiUrl): void
    {
        $this->googleMapsApiUrl = $googleMapsApiUrl;
    }

    /**
     * @param array $additionalOptions
     *
     * @return string
     */
    protected function generateGoogleMapsApiScriptUrl(array $additionalOptions = [])
    {
        if (!empty($this->getGoogleMapsApiUrl())/* && !empty($this->getGoogleApiKey())*/) {
            $additionalOptionsArray = [];
            if (!empty($additionalOptions)) {
                foreach ($additionalOptions as $k => $v) {
                    $additionalOptionsArray[] = $k . '=' . $v;
                }
            }
            return str_replace('?', '', $this->getGoogleMapsApiUrl()) .
                '?' . ($this->getGoogleApiKey() ? 'key=' . $this->getGoogleApiKey() . '&' : '') .
                implode('&', $additionalOptionsArray);
        }
        return '';
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException|\TYPO3\CMS\Core\Resource\Exception\InvalidFileException
     */
    public function render(): string
    {
        $this->setSettings($this->templateVariableContainer->get('settings'));
        $this->setGoogleApiKey($this->settings['google_api_key']);
        $this->setGoogleMapsApiUrl($this->settings['google_maps_api_url']);

        $this->assetCollector->addStyleSheet(
            'drk_coursesearch-googlemaps-css',
            'EXT:drk_coursesearch/Resources/Public/Css/googlemaps.css'
        );


        if ($this->settings['GDPR']) {
            $this->assetCollector->addInlineJavaScript(
                "drk_coursesearch-googlemaps-gdpr",
                "
                    $('#googlemaps_gdpr').click(function(){
                            $.getScript('/" .
                PathUtility::getPublicResourceWebPath (
                    'EXT:drk_coursesearch/Resources/Public/Scripts/googlemaps.js'
                )
                . "');
                            $('#googlemaps_consent').hide();
                           });
                ",
                [],
                ['priority' => false]
            );
        } else {
            $this->assetCollector->addJavaScript(
                'drk_coursesearch-googlemaps',
                'EXT:drk_coursesearch/Resources/Public/Scripts/googlemaps.js',
                [],
                ['priority' => false]
            );
        }

        $mapHtml = '';
        if (/*!empty($this->getGoogleApiKey()) && */ !empty($this->arguments['locations'])) {
            $mapHtml = '<div id="map" style="height:' . $this->arguments['height'] . 'px; width:' . $this->arguments['width'] . 'px;">';
            if ($this->settings['GDPR']) {
                $mapHtml .= '<div id="googlemaps_consent">
                <div><input class="button o-btn" id="googlemaps_gdpr" type="button" value="Karte anzeigen"></div>
                <div>Wenn Sie die Karte nutzen, werden Cookies aktiviert, die für die Nutzung von Google Maps nötig sind. Diese Cookies übertragen einzelne Nutzerdaten an Google Maps. Mit der Nutzung der Karte erklären Sie sich automatisch damit einverstanden.</div>
                </div>';
            }
            $mapHtml .= '</div>';
            $mapHtml .= '<script type="text/javascript">';
            $mapHtml .= "
                let google_maps_src = '" . $this->generateGoogleMapsApiScriptUrl() . "';
            ";

            $mapHtml .= "let googleImgPath = '".PathUtility::getPublicResourceWebPath (
                    'EXT:drk_coursesearch/Resources/Public/Images/'). "'; ";
            $mapHtml .= 'let locations = [';

            $locationArray = [];
            $lastMarkerLabelArray = [];
            foreach ($this->arguments['locations'] as $location) {
                if (in_array($location['markerLabel'], $lastMarkerLabelArray)) {
                    continue;
                }
                $locationArray[] = '[' .
                    '"",' .
                    $location[$this->arguments['latitudeField']] . ',' .
                    $location[$this->arguments['longitudeField']] .
                    ']';
                $lastMarkerLabelArray[$location['markerLabel']] = $location['markerLabel'];
            }
            $mapHtml .= implode(',', $locationArray);

            $mapHtml .= '];';
            $mapHtml .= '</script>';
        }
        return $mapHtml;
    }
}
