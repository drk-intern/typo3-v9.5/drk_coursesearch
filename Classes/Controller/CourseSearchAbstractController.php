<?php

namespace DRK\DrkCoursesearch\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkCoursesearch\Domain\Repository\CourseRepository;
use http\Exception\BadUrlException;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFileWritePermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileException;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

class CourseSearchAbstractController extends AbstractDrkController
{
    /**
     * @var CourseRepository
     */
    protected CourseRepository $courseRepository;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * @param CourseRepository $courseRepository
     */
    public function injectCourseRepository(CourseRepository $courseRepository): void
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * Init
     *
     * @return void
     * @throws InsufficientFileWritePermissionsException|ResourceDoesNotExistException
     * @throws InvalidFileException
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

         $this->assetCollector->addStyleSheet(
            'Coursersearch',
            'EXT:drk_coursesearch/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'Coursesearch',
            'EXT:drk_coursesearch/Resources/Public/Scripts/tx_drkcoursesearch.js',
            [],
            ['priority' => false]

        );

        if (!empty($this->settings['autocompleteSrc']) && intval($this->settings['autocompleteUse']) == 1) {

            $location_json = $this->getLocationFilePath();

            $javaScript = "
                $.getScript( '".PathUtility::getPublicResourceWebPath (
                    'EXT:drk_coursesearch/Resources/Public/Scripts/jquery.autocomplete.min.js') . "' " .
                ", function () {
                  'use strict';
                    $.getJSON('$location_json', function(data) {
                      $('#tx-drkcoursesearch-angebote-searchbox').autocomplete({
                        lookup: data,
                        minChars: 1,
                        showNoSuggestionNotice: false,
                      });
                    });
                });
            ";

            $this->assetCollector->addInlineJavaScript(
                'Coursesearch-autocomplete',
                $javaScript,
                [],
                ['priority' => false]


            );
        }
    }

    /**
     * @return string
     * @throws InsufficientFileWritePermissionsException|BadUrlException|ResourceDoesNotExistException
     */
    protected function getLocationFilePath(): string
    {
        $publicPath = Environment::getPublicPath();
        $jsonFile = $jsonFileContent = '';
        $src = $this->settings['autocompleteSrc'];

        $typo3tempDirectory = '/typo3temp/'  . $this->request->getControllerExtensionKey() . '/';
        GeneralUtility::mkdir_deep($publicPath . $typo3tempDirectory);
        $jsonFile = $typo3tempDirectory . 'city_list.json';

        if (file_exists($jsonFile)) {
            return $jsonFile;
        }
        else {
            if ($jsonFileContent = GeneralUtility::getUrl($src)) {

                if (!GeneralUtility::writeFile($publicPath . $jsonFile, $jsonFileContent, true)) {
                    throw new InsufficientFileWritePermissionsException('Autocomplete: Temporary file could not be created!');
                }
                else {
                    return $jsonFile;
                }

            }
            else {
                throw new ResourceDoesNotExistException('File could not be found! Wrong "autocompleteSrc"');
            }
        }
    }

    /**
     * @param string $strCsv
     * @return array
     */
    protected function explodeRangeCsv( string $strCsv = ""): array
    {
        $result = [];

        if (empty($strCsv)) {
            return $result;
        }

        $arrayCsv = explode(',', $strCsv);

        foreach ($arrayCsv as $value) {
            $result[$value] = $value . 'km';
        }

        return $result;

    }
    /**
     * @param string $preselectedCourseType
     */
    protected function assignCourseList(string $preselectedCourseType = ''): void
    {
        $courseList = $this->courseRepository->getCourseTypeList();

        $preselection = [];
        if (!empty($preselectedCourseType)) {
            foreach ($courseList as $course) {
                if ((int)$course['CourseType'] === (int)$preselectedCourseType) {
                    $preselection = $course;
                    break;
                }
            }
        }

        $this->view->assignMultiple([
            'courseList' => $courseList,
            'preselection' => $preselection,
        ]);
    }
}
