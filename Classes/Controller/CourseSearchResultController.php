<?php

namespace DRK\DrkCoursesearch\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


use Exception;
use TYPO3\CMS\Core\Pagination\ArrayPaginator;
use GeorgRinger\NumberedPagination\NumberedPagination;
use Psr\Http\Message\ResponseInterface;

class CourseSearchResultController extends CourseSearchAbstractController
{

    /**
     * courseResult view
     *
     * @param string $zip
     * @param string $courseType
     * @param int $iSearchRange
     * @return ResponseInterface
     * @throws Exception
     */
    public function courseResultAction(string $zip = '', string $courseType = 'K', int $iSearchRange = 0): ResponseInterface
    {
        //TODO: make tracker functional before implementing
        //$this->addTrackerJS();

        $courses = [];
        $organisation = [];
        $zipLengthError = (strlen($zip) < 3);
        $currentPage = $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1;

        $iSearchRange = $iSearchRange == 0 ? $this->settings['search_range'] ?? 5 : $iSearchRange;

        // select search range
        if (isset($this->settings['search_range_selection']) && !empty($this->settings['search_range_selection']))
        {
            $this->view->assign('searchRangeSelection', $this->explodeRangeCsv($this->settings['search_range_selection']));
        } else {
            $this->view->assign('searchRangeSelection', []);
        }

        $this->view->assign('searchRange', $iSearchRange);

        /**
         * check for valid zip code
         */
        if ($zipLengthError) {
            $this->assignCourseList($courseType);
            $this->view->assignMultiple([
                'zip' => $zip,
                'courseType' => $courseType,
                'courses' => [],
                'organisation' => $organisation,
                'zipLengthError' => true,
            ]);

            return $this->htmlResponse();
        }

        // if empty result list ask webservice
        $courseType = $courseType ?: '9999';
        $courses = $this->courseRepository->getCourses($courseType, $zip, $iSearchRange);
        $organisation = $this->courseRepository->getOrganisation($zip);

        $this->assignCourseList($courseType);
        $this->view->assignMultiple([
            'zip' => $zip,
            'courseType' => $courseType,
            'courses' => $courses,
            'organisation' => $organisation,
            'zipLengthError' => false,
        ]);

        /**
         * Set pagination
         */
        $itemsPerPage = 10;
        $maximumLinks = 15;

        $paginator = new ArrayPaginator($courses, $currentPage, $itemsPerPage);
        $pagination = new NumberedPagination($paginator, $maximumLinks);
        $this->view->assign('pagination', [
            'paginator' => $paginator,
            'pagination' => $pagination,
        ]);

        return $this->htmlResponse();
    }
}
