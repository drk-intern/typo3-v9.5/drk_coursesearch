<?php

namespace DRK\DrkCoursesearch\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Psr\Http\Message\ResponseInterface;

class CourseSearchController extends CourseSearchAbstractController
{
    /**
     * form view
     */
    public function formAction(): ResponseInterface
    {

        // select search range
        if (isset($this->settings['search_range_selection']) && !empty($this->settings['search_range_selection']))
        {
            $this->view->assign('searchRangeSelection', $this->explodeRangeCsv($this->settings['search_range_selection']));
        } else {
            $this->view->assign('searchRangeSelection', []);
        }

        $this->assignCourseList($this->settings['preselection'] ?? '');
        $this->view->assign('arguments', $this->request->getArguments());
        $this->view->assign('showLabels', $this->settings['show_label'] ?? null);

        return $this->htmlResponse();
    }
}
