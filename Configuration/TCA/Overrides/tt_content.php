<?php

// Coursesearch Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_coursesearch',
    'Coursesearch',
    'LLL:EXT:drk_coursesearch/Resources/Private/Language/locallang_be.xlf:tt_content.coursesearch_plugin.title',
    'EXT:drk_coursesearch/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kurstermine'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_coursesearch/Configuration/FlexForms/coursesearch.xml',
    // ctype
    'drkcoursesearch_coursesearch'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers
            --div--;Einstellungen,
            pi_flexform',

    ]
);

// CoursesearchResult Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_coursesearch',
    'CoursesearchResult',
    'LLL:EXT:drk_coursesearch/Resources/Private/Language/locallang_be.xlf:tt_content.coursesearchresult_plugin.title',
    'EXT:drk_coursesearch/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kurstermine'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:drk_coursesearch/Configuration/FlexForms/coursesearchresult.xml',
    // ctype
    'drkcoursesearch_coursesearchresult'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

